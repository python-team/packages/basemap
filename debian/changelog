basemap (1.4.1-1) unstable; urgency=medium

  [ Alexandre Detiste ]
  * use new dh-sequence-python3
  * apply the Multi-Arch hint

  [ Emmanuel Arias ]
  * d/copyright: Remove Files-Excluded they are not present in upstream tarball.
  * d/watch: Remove repacksuffix, it's not necessary for now.
  * New upstream version (Closes: #1076957, #1074839).
  * d/patches: Update patches according to new upstream release.
    - d/patches/dont_install_data_files: Remove patch is not longer needed.
    - d/patches/use_share_datafiles: Update patch according to new upstream
      release.
  * d/patches/reproducible_documentation.patch: Update patche to get
    correctly the conf.py file.
    - Update according to new upstream release.
  * d/control: Add furo as Build-Depends.
  * d/patches/fix-docs-links.patch: Fix links in documentation. Remove plots
    that use internet.
  * d/control: Replace cython3-legacy by cython3 in Build Dependencies (Closes:
    #1092778).
  * d/rules: Update folder where setup.py is located.
  * d/rules: Add hack to move index.rst from doc/source/ to doc/ folder.
  * Update source path in d/python-mpltoolkits.basemap-data.install and
    d/python-mpltoolkits.basemap-doc.install files.
    - Include basemap_data_hires package in python-mpltoolkits.basemap-data.
  * d/tests/run-unit-tests: Ignore test.py streamplot_demo.py warpimage.py
    geos_demo_3.py tests, they are failling and need more investigation.
  * d/rules: remove .debhelper folder during dh_auto_clean.
  * d/control: Add python3-numpy2-abi0 as B-Depends.
  * d/control: Update my contact information.
  * d/copyright: Update path to files. Remove reference to src/c_numpy.pxd, it
    not longer in the source.

 -- Emmanuel Arias <eamanu@debian.org>  Fri, 30 Aug 2024 13:21:19 -0300

basemap (1.2.2+dfsg-5) unstable; urgency=medium

  * Do not fail in separate arch-all builds. Closes: #1068145.
  * Apply patch for d/rules from bug #790235

 -- Andreas Tille <tille@debian.org>  Wed, 27 Mar 2024 07:03:11 +0100

basemap (1.2.2+dfsg-4) unstable; urgency=medium

  * Team upload

  [ Andreas Tille ]
  * Add me and Emmanuel Arias to Uploaders
    Closes: #1065235
  * Standards-Version: 4.6.2 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Remove field Testsuite on binary package python-mpltoolkits.basemap-doc
    that duplicates source.
  * Add lintian-overrides for custom-library-search-path
  * Drop unneeded custom compression dh_builddeb -- -Zxz
  * Clarify copyright statements
  * Add autopkgtest
    Closes: #1035126, #1036514
  * Apply patch to make documentation reproducible (hopefully)
    Affects bug #790235 but probably does not close it
  * Adapt filter name to a value available in PIL10
  * Use np.percentile instead of removed plt.prctile_rank
  * Hardening
  * Fix permissions of data
  * Use float instead of deprecated np.float
  * Replace deprecated clock() by perf_counter() to measure time needed
  * Use python3-packaging instead of distutils
  * Fix clean target
  * Lintian-overrides for source-ships-excluded-file

  [ Bas Couwenberg ]
  * Switch to cython3-legacy
    Closes: #1056789

  [ Emmanuel Arias ]
  * d/patches/0004-Move-the-imports-in-all-.py-files.patch: Add patch to
    replace all that move the imports to avoid make a reference to
    mpl_toolkits that is already installed by matplotlib.
    - d/rules: Update PYTHONPATH tu set it on the build/*/mpl_toolkits
      folder.
    Closes: #1009424

 -- Andreas Tille <tille@debian.org>  Tue, 26 Mar 2024 22:14:00 +0100

basemap (1.2.2+dfsg-3) unstable; urgency=medium

  * orphan

 -- Sandro Tosi <morph@debian.org>  Sat, 02 Mar 2024 01:31:09 -0500

basemap (1.2.2+dfsg-2) unstable; urgency=medium

  * remove -dbg package; Closes: #994296

 -- Sandro Tosi <morph@debian.org>  Thu, 30 Sep 2021 20:00:41 -0400

basemap (1.2.2+dfsg-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * debian/copyright: Replace commas with whitespace to separate items in Files
    paragraph.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.5.0, no changes needed.

  [ Stefano Rivera ]
  * Cython before building, not after building before installing.
    (Closes: #971916)
  * d/control: Update Uploader field with new Debian Python Team
    contact address.

  [ Sandro Tosi ]
  * New upstream release; Closes: #966969
  * debian/patches
    - remove patches merged upstream

 -- Sandro Tosi <morph@debian.org>  Sun, 18 Oct 2020 19:15:00 -0400

basemap (1.2.1+dfsg-1) unstable; urgency=medium

  * New upstream release; Closes: #939128
  * debian/control
    - add deprecation warning in favor of cartopy
  * debian/copyright
    - udpate for new upstream release
  * debian/patches/use_share_datafiles
    - refresh patch

 -- Sandro Tosi <morph@debian.org>  Thu, 05 Sep 2019 22:20:20 -0400

basemap (1.2.0+dfsg-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0.

  [ Sandro Tosi ]
  * Drop support for python 2; Closes: #932614, #932655, #932657
  * debian/{control, compat}
    - bump compat to 9
  * debian/control
    - drop python 2 packages
    - wrap-and-sort
    - add pil to b-d, needed by doc
  * debian/rules
    - use pybuild
    - use dh sequence for build target
    - build doc with py3k
  * debian/patches/doc_make.py_py3k.patch
    - port doc/make.py to py3k

 -- Sandro Tosi <morph@debian.org>  Tue, 20 Aug 2019 13:12:12 -0400

basemap (1.2.0+dfsg-1) unstable; urgency=medium

  * New upstream release; Closes: #918643
  * debian/watch
    - match tags with `rel` suffix
  * debian/copyright
    - extend packaging copyright years
  * debian/patches/*
    - refresh patches
  * debian/control
    - add setuptool, six, pyshp, pyproj, matplotlib, netcdf4 to b-d
    - bump Standards-Version to 4.3.0 (no changes needed)
  * debian/control
    - rework for new upstream setup.py

 -- Sandro Tosi <morph@debian.org>  Thu, 10 Jan 2019 17:53:21 -0500

basemap (1.1.0+dfsg-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

  [ Sandro Tosi ]
  * Acknowledge NMU; Closes: #903579

 -- Sandro Tosi <morph@debian.org>  Thu, 29 Nov 2018 18:45:55 -0500

basemap (1.1.0+dfsg-2.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Julian Taylor ]
  * Re-cythonize the sources at build time.  Closes: #903579
  * Fix path to README.md in d/docs.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 28 Oct 2018 17:23:31 +0100

basemap (1.1.0+dfsg-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Sandro Tosi ]
  * debian/control
    - bump Standards-Version to 4.1.4 (no changes needed)
  * debian/rules
    - fail build if any python3 installation fails; Closes: #866906

 -- Sandro Tosi <morph@debian.org>  Sun, 13 May 2018 21:30:05 -0400

basemap (1.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release; Closes: #869156
  * debian/control
    - switch to PIL instead of imaging; #866415
    - bump Standards-Version to 4.1.3
    - remove priority: extra from -dbg packages
    - add pyproj and pyshp to dep
    - add netcdf4, pyproj and pyshp to b-d, needed by doc
  * debian/copyright
    - extend packaging copyright years
    - exclude `geos-3.3.3` from upstream tarballs
    - update for removed upstream code
  * debian/watch
    - update to point to github

 -- Sandro Tosi <morph@debian.org>  Mon, 22 Jan 2018 23:21:34 -0500

basemap (1.0.7+dfsg-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Sandro Tosi ]
  * debian/rules
    - add to build-indep a dependency on build-arch; Closes: #806606
  * debian/control
    - bump Standards-Version to 3.9.8 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Sun, 14 Aug 2016 17:39:28 +0100

basemap (1.0.7+dfsg-3) unstable; urgency=medium

  * debian/control
    - set me as Maintainer, team as Uploaders

 -- Sandro Tosi <morph@debian.org>  Sun, 02 Aug 2015 20:43:53 +0100

basemap (1.0.7+dfsg-2) experimental; urgency=medium

  * debian/control
    - added scipy to Suggests, needed for some interpolation routines
    - added python-owslib to Suggests, needed to retrieve images using WMS
      servers
    - bump Standards-Version to 3.9.6 (no changes needed)
    - update Homepage field
  * Switch to dh_python2
  * Introduce a proper -dbg package
  * Add py3k packages; thanks to Eike von Seggern for report; Closes: #776604
  * debian/copyright
    - extend packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Sat, 07 Feb 2015 15:15:57 +0000

basemap (1.0.7+dfsg-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Add Vcs-* fields.
  * Fix a typo in the package description.

  [ Sandro Tosi ]
  * New upstream release
  * patch refreshed
  * debian/copyright
    - extended packaging copyright years
    - use the right URL for Format header
    - added reference to local file for GPL-2 license
  * debian/control
    - bump Standards-Version to 3.9.5 (no changes needed)
    - update Homepage field
  * debian/watch
    - mangle local version

 -- Sandro Tosi <morph@debian.org>  Sat, 11 Jan 2014 22:12:05 +0100

basemap (1.0.6+dfsg-1) experimental; urgency=low

  * new upstream release
  * debian/copyright
    - extended Debian packaging years
    - added new upstream files
  * debian/patches/{fix_ftbfs_with_geos3.3, use_share_datafiles}
    - updated to new upstream code

 -- Sandro Tosi <morph@debian.org>  Fri, 12 Apr 2013 22:57:26 +0200

basemap (1.0.5+dfsg-1) experimental; urgency=low

  * new upstream release
  * debian/patches/*
    - refresh patches
  * debian/control
    - add python-tk to b-d, needed to have a fully functional matplotlib module
  * debian/rules
    - fix a FTBFS when building the documentation
  * debian/patches/use_share_datafiles
    - use BASEMAPDATA variable also in pyproj.py, so that we can pass a local
      directory when building doc

 -- Sandro Tosi <morph@debian.org>  Fri, 10 Aug 2012 23:46:23 +0200

basemap (1.0.3+dfsg-2) unstable; urgency=low

  * debian/rules
    - set MPLCONFIGDIR to a writable directory, so it won't fail to build the
      doc; thanks to Lucas Nussbaum for the report; Closes: #676058

 -- Sandro Tosi <morph@debian.org>  Fri, 15 Jun 2012 19:51:28 +0200

basemap (1.0.3+dfsg-1) unstable; urgency=low

  * New upstream release
    - removed geos-3.3.3 from tarball, hence the dfsg tag
  * debian/copyright
    - updated for new upstream code
    - extended packaging copyright years
  * debian/patches/dont_compile_nad2bin_on_clean
    - removed, merged upstream
  * debian/patches/use_share_datafiles
    - updated for new upstream code

 -- Sandro Tosi <morph@debian.org>  Mon, 28 May 2012 20:07:00 +0200

basemap (1.0.2-3) unstable; urgency=low

  * debian/rules
    - remove leftover from build process, so the package can be built twice in a
      row; thanks to Jakub Wilk for the report; Closes: #671162
    - use xz compression; thanks Julian Taylor for the report and patch;
      Closes: #654651

 -- Sandro Tosi <morph@debian.org>  Fri, 04 May 2012 21:49:22 +0200

basemap (1.0.2-2) unstable; urgency=low

  * debian/patches/fix_ftbfs_with_geos3.3
    - fix a FTBFS with geos 3.3; thanks to Andreas Beckmann for the report and
      to Francesco P. Lovergine for the patch; Closes: #663403
  * debian/{control, rules}
    - use dh_numpy
  * debian/control
    - bump Standards-Version to 3.9.3 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Sat, 17 Mar 2012 16:51:34 +0100

basemap (1.0.2-1) unstable; urgency=low

  * New upstream release
  * debian/copyright
    - updated
  * debian/patches/configure_third_party_modules_installation
    - removed, no longer needed
  * debian/patches/{dont_install_data_files, use_share_datafiles,
                    dont_compile_nad2bin_on_clean}
    - updated to new upstream code

 -- Sandro Tosi <morph@debian.org>  Sun, 13 Nov 2011 22:50:58 +0100

basemap (1.0.1-1) unstable; urgency=low

  * Initial release (Closes: #389638)

 -- Sandro Tosi <morph@debian.org>  Wed, 21 Sep 2011 20:54:38 +0200
