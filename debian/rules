#!/usr/bin/make -f

export PYBUILD_SYSTEM=custom

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

PY3VERS := $(shell py3versions -s)
LIB := $$(python3 -c "from setuptools.command.build import build ; from setuptools import Distribution ; b = build(Distribution()) ; b.finalize_options() ; print(b.build_platlib)")
# This should have the same result without using setuptools - but it fails creating the docs
# LIB := $$(find build -type d -name mpl_toolkits | grep `py3versions -d -v | sed 's/\.//'` | sed 's?/mpl_toolkits??')

%:
	dh $@ --with sphinxdoc --buildsystem=pybuild

override_dh_auto_install:
	set -e ;
	cd $(CURDIR)/packages/basemap/ ;\
	for python in $(PY3VERS); do \
		$$python setup.py install --root $(CURDIR)/debian/python3-mpltoolkits.basemap/ --install-layout=deb; \
	done

override_dh_auto_build:
	set -e ; \
	cd packages/basemap/ \
	cython3 -3 src/*pyx; \
	for python in $(PY3VERS); do \
		$$python setup.py build; \
	done

	# HACK: we need to import axes_grid1 but we need to import it from the "local"
	# mpl_toolkits namespace
	ln -s $$(dirname $$(python3 -c "import mpl_toolkits.axes_grid1 as p; print(p.__file__)")) $(CURDIR)/packages/basemap/$(LIB)/mpl_toolkits/
	# build doc only for default python version
	#mv $(CURDIR)/packages/basemap/$(LIB)/*.so $(CURDIR)/packages/basemap/$(LIB)/mpl_toolkits/
	mv $(CURDIR)/packages/basemap/doc/source/index.rst $(CURDIR)/packages/basemap/doc/index.rst
	(export MPLCONFIGDIR=. ; cd packages/basemap/doc ; PYTHONPATH=$(CURDIR)/packages/basemap/$(LIB) BASEMAPDATA=$(CURDIR)/packages/basemap_data/src/mpl_toolkits/basemap_data/ python3 make.py html)
	# remove hack
	rm $(CURDIR)/packages/basemap/$(LIB)/mpl_toolkits/axes_grid1
	mv $(CURDIR)/packages/basemap/doc/index.rst $(CURDIR)/packages/basemap/doc/source/

override_dh_python3:
	dh_python3
	-chmod -x debian/python-mpltoolkits.basemap-data/usr/share/basemap/data/*
	dh_numpy3

override_dh_sphinxdoc:
	dh_sphinxdoc -i

override_dh_clean:
	rm -rf debian/.debhelper/

	set -e; \
	cd packages/basemap/

	rm -rf doc/build
	rm -f nad2bin.o nad2bin
